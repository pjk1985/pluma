import qbs

Project{
    references: "../src/Pluma.qbs"

    property bool install: true

    CppApplication {
        name: "Host"
        cpp.includePaths:[
            "./interface",
            "../include",
        ]
        consoleApplication: true

        files: [
            "host/Main.cpp",
            "host/SimpleWarrior.hpp",
            "interface/Warrior.cpp",
            "interface/Warrior.hpp",
        ]

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: project.install
            qbs.installDir: "bin"
        }

        Depends{
            name: "Pluma"
        }
    }

    DynamicLibrary{
        name: "Plugin"

        Depends { name: "cpp" }

        files: [
            "interface/Warrior.cpp",
            "interface/Warrior.hpp",
            "plugin/Connector.cpp",
            "plugin/Eagle.hpp",
            "plugin/Jaguar.hpp",
        ]

        install: project.install
        installDir: "bin/plugins"

        cpp.includePaths:[
            "./interface",
            "../include",
        ]
        Depends{
            name: "Pluma"
        }
        Depends{
            name: "Host"
        }
    }
}
