import qbs.FileInfo

DynamicLibrary {
    name:"Pluma"

    Depends { name: "cpp" }

    cpp.cxxLanguageVersion: "c++11"
    cpp.defines: [
        "PLUMA_LIBRARY",
        "PLUMA_EXPORTS"
    ]
    cpp.includePaths: [".", "../include"]

    files: [
        "Pluma/DLibrary.cpp",
        "Pluma/Dir.cpp",
        "Pluma/Host.cpp",
        "Pluma/PluginManager.cpp",
        "Pluma/Provider.cpp",
    ]

    // Default rules for deployment.
    qbs.installPrefix: ""
    Properties {
        condition: qbs.targetOS.contains("unix")
        install: false
        installDir: "/usr/lib"
    }
    Properties {
        condition: qbs.targetOS.contains("linux")
        cpp.driverLinkerFlags : "-ldl"
    }
}
